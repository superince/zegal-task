## Real Time Quote Notification App

### Brief description
*This application is created with Node Express with typescipt. Rabbitmq is used as a message broke to publish and subscribe, socket.io to push the messages to the React frontend and finally docker to containerize all the services.*

### Tech Stack 
- Node,Express,Socket.io
- React

### Steps to run the application in development env
1. Make a copy .env.example to .env in both frontend and backend folder
2. Added neccessary variable to the .env file
3. Make sure your system has docker installed correctly and run the following command
```
sudo docker-compose -f docker-compose-local.yml up
```
4. Navigate to http://localhost:3000

### Steps to run the application in production env
1. Make sure your system has docker installed correctly and run the following command
```
sudo docker-compose up
```
2. Navigate to http://localhost:3000

### Backend environment variable description
| Name | Description |
| ------ | ------ |
| NODE_ENV | Application environment(local,development,production) |
| APP_PORT | Application running port |
| LOG_LEVEL | Winston package log specified level(silly,info,debug) |
| RABBITMQ_URI | Connection URL of RabbitMq service|

### Frontend environment variable description
| Name | Description |
| ------ | ------ |
| REACT_APP_SOCKET_URI | Socket connection URL |


### Application Dependencies:

| Dependency Name | Package Version | Description |
| ------ | ------ | ------ |
| express | "express": "^4.18.2" | Nodejs web framework |
| winston | "winston": "^3.8.2" | Application Logger |
| amqplib | "amqplib": "^0.10.3" | RabbitMq connector |
| dotenv | "dotenv": "^16.0.3" | For enviroment managment |
| axios | axios": "^1.3.6" | External request calling library |
| cors | "cors": "^2.8.5" | Cross origin resource sharing |
| node-cron | "node-cron": "^3.0.2" | Timly runing cron job  |
| socket.io | "socket.io": "^4.6.1" | Package to maintain socket connection |
| jest | "jest": "^29.5.0" | Testing library  |

### Basic work flow diagram
![My Image](work-diagram.png)
