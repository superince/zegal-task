export interface IMessage {
  message: string;
  timestamp: string;
  priority: number;
}