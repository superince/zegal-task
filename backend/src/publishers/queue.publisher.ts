import { QUEUE_NAME } from '../constants/constants';
import { channel } from '../config/queue';

export const publish = (message: string) => {
  channel.sendToQueue(QUEUE_NAME, Buffer.from(message));
}