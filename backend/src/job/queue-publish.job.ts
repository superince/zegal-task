import cron from 'node-cron';
import * as QueuePublisher from '../publishers/queue.publisher';
import Logger from '../config/logger';
import { getQuote } from '../utils/api.util';
import { generateRandomNum } from '../utils/rand.util';
import { AxiosResponse } from 'axios';
import { IMessage } from '@/interfaces/IMessage';

export const start = async () => {
  cron.schedule('*/1 * * * *', async () => {
    Logger.info(`Queue publish job started`);
    const promiseArr: Promise<AxiosResponse<any, any>>[] = [];
    for (let i = 0; i < 20; i++) {
      promiseArr.push(getQuote());
    }
    try {
      const responses = await Promise.all(promiseArr);
      responses.forEach(({ data }) => {
        const message: IMessage = {
          message: data.content,
          timestamp: new Date().toISOString(),
          priority: generateRandomNum()
        }
        QueuePublisher.publish(JSON.stringify(message))
      });
    } catch (e) {
      Logger.error('Error occurred', e);
    }
    Logger.info(`Queue publish job ended`);
  });
}