import 'jest';
import supertest from "supertest";
import server from './config/express';

describe("Server", () => {
  const request = supertest.agent(server);
  it("should get /", async () => {
    const res = await request.get("/");

    expect(res.status).toBe(200)
    expect(res.body).toEqual({ "message": "Server is up and running." })
  });
});

describe("Server", () => {
  const request = supertest.agent(server);
  it("should get throw error on undefined routes", async () => {
    const res = await request.get("/about");

    expect(res.status).toBe(400)
    expect(res.body).toEqual({  "error": "No such route found" })
  });
});