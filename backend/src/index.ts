import httpServer from './config/express';
import dotenv from 'dotenv';
import Logger from './config/logger';
import * as QueuePublishJob from './job/queue-publish.job';
import { createQueueConnection } from './config/queue';
dotenv.config()

const startServer = async () => {
  try {
    await createQueueConnection();
    QueuePublishJob.start();
    httpServer.listen(process.env.APP_PORT, () => {
      Logger.info(`##############################🛡️  Server listening on port: ${process.env.APP_PORT} 🛡️ ##############################`);
    })
  } catch (e) {
    Logger.error(e);
    process.exit(1);
  }
}

startServer();