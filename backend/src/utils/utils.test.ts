import { expect, describe, it } from '@jest/globals';
import { generateRandomNum } from './rand.util';
import { getQuote } from './api.util';

describe("Utilities test", () => {
  it("should get random number", async () => {
    const result = generateRandomNum();

    expect(result).not.toBeNull
    expect(typeof result).toBe("number");
  });
});

describe("Utilities test", () => {
  it("should get throw error on undefined routes", async () => {
    const result = getQuote();

    expect(result).not.toBeNull
    expect(typeof result).toBe("object")
  });
});