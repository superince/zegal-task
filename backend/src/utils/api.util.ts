import axios, { AxiosResponse } from 'axios';
import { QUOTE_URI } from '../constants/constants';

export const getQuote = (): Promise<AxiosResponse<any, any>> => {
  return axios.get(QUOTE_URI);
}