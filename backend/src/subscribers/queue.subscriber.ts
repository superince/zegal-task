import { IMessage } from '@/interfaces/IMessage';
import { Channel, ConsumeMessage } from 'amqplib';
import Logger from '../config/logger';
import { sendMessage } from '../config/socket'

export const consumer = (channel: Channel) => (msg: ConsumeMessage | null): void => {
  if (msg) {
    const data: string = msg.content.toString()
    try {
      const jsonData: IMessage = JSON.parse(data)
      if (jsonData.priority >= 7) {
        sendMessage(data)
      }
    } catch (e) {
      Logger.error(`Failed to parse json. ${e}`)
    } finally {
      channel.ack(msg);
    }
  }
}