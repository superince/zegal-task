import client, { Channel, Connection } from 'amqplib';
import { consumer } from '../subscribers/queue.subscriber';
import { QUEUE_NAME } from '../constants/constants';

export let channel: Channel = null;

export const createQueueConnection = async () => {
  const connection: Connection = await client.connect(
    process.env.RABBITMQ_URI
  );
  
  // Create a channel
  channel = await connection.createChannel();

  // Create a queue
  await channel.assertQueue(QUEUE_NAME);

  // Consume messages from queue
  await channel.consume(QUEUE_NAME, consumer(channel))
}