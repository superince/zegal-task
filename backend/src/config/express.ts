import http, { Server as HttpServer } from 'http';
import express, { Application, Request, Response } from 'express';
import cors from 'cors';
import * as SocketServer from './socket';

const app: Application = express();
app.use(cors());

const httpServer: HttpServer = http.createServer(app);
SocketServer.setupSocket(httpServer);

app.get('/', (req: Request, res: Response) => {
  return res.status(200).json({
    "message": "Server is up and running."
  })
})

app.use("*", (req: Request, res: Response) => {
  return res.status(400).json({
    "error": "No such route found"
  })
})

export default httpServer;