import Logger from '../config/logger';
import { Server as HttpServer } from 'http';
import { Socket, Server as SocketServer } from "socket.io";

const totalClients: Array<Socket> = [];

const connectionHandler = (client: Socket) => {
  Logger.info(`Client Connected`);
  totalClients.push(client);
  client.on('disconnect', () => disconnectHandler(client))
}

const disconnectHandler = (client: Socket) => {
  const index = totalClients.indexOf(client)
  if (index > -1) {
    totalClients.splice(index, 1);
  }
  Logger.info(`Client disconnected`);
}

export const sendMessage = (message: string) => {
  totalClients.forEach((client) => {
    client.emit('message', message)
  })
}

export const setupSocket = (httpServer: HttpServer) => {
  const io = new SocketServer(httpServer);
  io.on('connection', connectionHandler)
}