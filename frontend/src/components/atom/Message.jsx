import styled from 'styled-components'

const H1 = styled.h1`
  font-size: 16px;
  font-weight:400;
`
const Message = ({ message }) => {
  return (
    <H1>{message}</H1>
  )
}

export default Message;;