import styled from 'styled-components'
import * as DateUtils from '../../utils/DateUtils';

const StyledSpan = styled.span`
  font-size: 14px;
  font-weight:400;
  color:grey;
  margin:8px 0px;
`
const Timestamp = ({timestamp}) => {
  return (
    <StyledSpan className="timestamp">{DateUtils.getFormattedDate(timestamp,'longDateFormat')}</StyledSpan>
  )
}

export default Timestamp;