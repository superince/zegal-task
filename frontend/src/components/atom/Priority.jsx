import styled from 'styled-components'

const StyledSpan = styled.span`
  font-size: 15px;
  font-weight:400;
  color:grey;
  margin:8px 0px;
`
const Priority = ({ priority }) => {
  return (
    <StyledSpan className="priority">Priority : {priority}</StyledSpan>
  )
}

export default Priority;