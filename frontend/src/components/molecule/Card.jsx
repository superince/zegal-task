import styled from 'styled-components'
import { Message, Priority, Timestamp } from '../atom'

const StyledDiv = styled.div`
  display: flex;
  flex-direction:column;
  border-radius:4px;
  margin:16px 0px;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  padding:12px;
  background: white;
`
const FlexDiv = styled.div`
  display:flex;
  justify-content:space-between;
`
const Card = ({ message }) => {
  return (
    <StyledDiv>
      <Message message={message.message} />
      <FlexDiv>
        <Priority priority={message.priority} />
        <Timestamp timestamp={message.timestamp} />
      </FlexDiv>
    </StyledDiv>
  )
}

export default Card