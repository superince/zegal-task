import { useEffect, useState } from 'react';

const useSocket = (socket) => {
  const [isConnected, setIsConnected] = useState(socket.connected);
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    socket.on('connect', onConnected);
    socket.on('disconnect', onDisconnect);
    socket.on('message', onMessageReceived);

    return () => {
      socket.off('connect', onConnected);
      socket.off('disconnect', onDisconnect);
      socket.off('message', onMessageReceived);
    }
    // eslint-disable-next-line
  }, []);

  const onConnected = () => {
    setIsConnected(true);
  }

  const onDisconnect = () => {
    setIsConnected(false);
  }

  const onMessageReceived = (message) => {
    const jsonData = JSON.parse(message)
    setMessages(messages => [jsonData, ...messages])
  }

  return {
    isConnected,
    messages
  }
}

export default useSocket