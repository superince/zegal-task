import moment from 'moment';

const dateFormats = {
  longDateFormat: 'lll'
}

export const getFormattedDate = (isoFormatDate, format) => moment(isoFormatDate).format(dateFormats[format]);