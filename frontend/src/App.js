import { socket } from './socket';
import { useSocket } from './hooks'
import Card from './components/molecule/Card';

const App = () => {
  const {
    isConnected,
    messages
  } = useSocket(socket)

  return (
    <div className='container'>
      {isConnected ?
        "Connected to Socket" : "Socket disconnected"
      }
      <div>
        {messages.map((message, index) => (<Card key={index} message={message} />))}
      </div>
    </div>
  );
}

export default App